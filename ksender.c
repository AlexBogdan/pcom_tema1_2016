#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "lib.h"

#define HOST "127.0.0.1"
#define PORT 10000

// Ne salvam parametrii de transmisie
unsigned char c_seq = 0, c_maxl = 250, c_time=5, c_npad = 0x00, c_padc = 0x00, c_eol = 0x0D;

// Se trimit parametrii transmisiei si se asteapta un raspuns
int initSender() {
    kermit_pack *t = initTransmision(c_seq, 'S', c_maxl, c_time, c_npad, c_padc, c_eol);
    
    sendPack(t);
    kermit_pack *r = waitForPack(c_time);
    if (r == NULL)
        return 0;
    if(r->type == 'Y') {
        c_seq = r->seq + 1;
        return 1;
    }
    return 0;
}

int sendFileName (char* fileName) {
    kermit_pack *r, *t; 
    int i=1;
    while(i <= 3){
        t = generatePack(c_seq, 'F', fileName, strlen(fileName));
        sendPack(t);
        r = waitForPack(c_time);
        if (r == NULL){
            i++;
            continue;
        }
        if (r->type == 'N') {
            c_seq = r->seq + 1;
            continue;
        }
        if (r->type == 'Y') {
            c_seq = r->seq + 1;
            return 1;
        }
    }
    return 0;
}

int main(int argc, char** argv) {
   
    init(HOST, PORT);
    int fileNo, fileDescriptor, readDescriptor, i;
    char buffer[c_maxl];
    kermit_pack *r, *t = NULL;

/*
    -=-=-=-=-=--=-=-=-==-= TRANSMISION BEGIN -=-=-=-=-=-=-=-=--=-=
    Trimiterea pachetului S pentru inceperea transmisiei
*/
    if (initSender() == 0){
        perror("");
        perror("Nu am putut stabili conexiunea cu receptorul.");
        return -1;
    }

/*
    -=-=-=-=-=-=-=-=-=-=--=-=- SEND FILES -=-=-=-=-=-=-=-=-=-=---=
*/
    for (fileNo=1; fileNo < argc; fileNo++){
        fileDescriptor = open(argv[fileNo], O_RDONLY);
        if (fileDescriptor < 0){
            perror("Fisierul nu exista sau nu poate fi deschis.");
            return -2;
        }
        if (sendFileName(argv[fileNo]) == 0){
            perror("Numele fisierului nu a putut fi transmis.");
            return -3;
        }

        do {
            memset(buffer, 0, c_maxl); // Avem grija sa nu trimitem informatie aiurea
            readDescriptor = read (fileDescriptor, buffer, c_maxl-5); // Citim cat de multi bytes putem
            if (readDescriptor < 0) {
                perror("Nu am putut citi din fisier.");
                return -1;
            }
            if (readDescriptor == 0) {
                close (fileDescriptor);
                break; // Nu mai avem nimic de citit
            }
            i=1;
            while(i <= 3){
                if (i == 4){
                    return -4;
                }
                t = generatePack(c_seq, 'D', buffer, readDescriptor);
                sendPack(t);
                r = waitForPack(c_time);//Verificare ACK / NAK
                if (r == NULL) {
                    i++;
                    continue;
                }
                if (r->type == 'N'){
                    c_seq = r->seq + 1;
                    continue;
                }
                if (r->type == 'Y') {
                    break;
                }
            }
            c_seq = r->seq + 1;
        } while(1);

        i=1;
        while(1){
            if (i == 4){
                return -4;
            }
            t = generatePack(c_seq, 'Z', NULL, 0);
            sendPack(t);
            r = waitForPack(c_time);//Verificare ACK / NAK
            if (r == NULL) {
                i++;
                continue;
            }
            if (r->type == 'N'){
                c_seq = r->seq + 1;
                continue;
            }
            if (r->type == 'Y') {
                break;
            }
        }
        c_seq = r->seq + 1;
    }

    i=1;
    while(1){
        if (i == 4){
            return -4;
        }
        t = generatePack(c_seq, 'B', NULL, 0);
        sendPack(t);
        r = waitForPack(c_time);//Verificare ACK / NAK
        if (r == NULL) {
            i++;
            continue;
        }
        if (r->type == 'N'){
            c_seq = r->seq + 1;
            continue;
        }
        if (r->type == 'Y') {
            break;
        }
    }
    c_seq = r->seq + 1;

    return 0;
}
