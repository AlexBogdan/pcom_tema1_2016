#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "lib.h"

#define HOST "127.0.0.1"
#define PORT 10001

// Ne salvam parametrii de transmisie
unsigned char c_seq = 0, c_maxl = 250, c_time=5, c_npad = 0x00, c_padc = 0x00, c_eol = 0x0D;

// Se trimit parametrii transmisiei dupa ce ii primeste pe cei ai transmitatorului
int initReceiver () {
    kermit_pack *r = waitForPack(c_time*3); // Asteptam de 3 ori timpul maxim
    if (r == NULL){
        return 0;
    }

    if (r->type == 'S') {
        c_seq = r->seq +1;
        kermit_pack *t = initTransmision(c_seq, 'Y', c_maxl, c_time, c_npad, c_padc, c_eol);
        sendPack (t);
        return 1;
    }
    return 0;
}

/* 
    Returneaza un descriptor pentru fisierul primit 
    in caz ca a fost primit pachetul corespunzator
    In fileName vom crea numelui noului fisier.
*/
int createFile (kermit_pack *p) {
    if (p->type == 'F'){
        char fileName[5 + p->len + 1]; // recv_ + fileName
        strcpy(fileName, "recv_");
        memcpy(&fileName[5], p->data, p->len);

        c_seq = p->seq + 1;
        return open(fileName, O_CREAT | O_WRONLY, 0777);
    }
    return -1;
}

int main(int argc, char** argv) {
    init(HOST, PORT);
    int fileDescriptor, i;
    kermit_pack *r, *t;
    
/*
    -=-=-=-=-=--=-=-=-==-= TRANSMISION BEGIN -=-=-=-=-=-=-=-=--=-=
    Trimiterea pachetuletului S pentru inceperea transmisiei
*/
    if (initReceiver() == 0){
        perror("");
        perror("Nu am putut stabili conexiunea cu emitorul.");
        return -1;
    }

    do {
        i=1;
        for (i = 1 ; i<= 3; i++){
            r = waitForPack(c_time);
            if (r != NULL){
                break;
            }
        }
        if (i==4){
            return -2;
        }
        if (r->check != checkSum(r)){
            c_seq = r->seq + 1;
            t = generatePack(c_seq, 'N', NULL, 0);
            sendPack(t);
            continue;
        }
        c_seq = r->seq + 1;
        switch (r->type) {
            case 'F' : 
                fileDescriptor = createFile(r); 
                if (fileDescriptor > 0){
                    t = generatePack(c_seq, 'Y', NULL, 0);
                    sendPack(t);
                } 
                else {
                    t = generatePack(c_seq, 'N', NULL, 0);
                    sendPack(t);
                }
                break;
            case 'D' :
                if (r->len - 5 > 0){
                    if (write (fileDescriptor, r->data, r->len - 5) < 0){
                        perror("Nu am putut scrie in fisier..");
                    }
                }
                t = generatePack(c_seq, 'Y', NULL, 0);
                sendPack(t);
                break;
            case 'Z' :
                t = generatePack(c_seq, 'Y', NULL, 0);
                sendPack(t);
                break;
            case 'B' :
                t = generatePack(c_seq, 'Y', NULL, 0);
                sendPack(t);
                return 0;
        }
    } while(1);
    
	return 0;
}
