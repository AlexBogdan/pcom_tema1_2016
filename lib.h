#ifndef LIB
#define LIB

#include <string.h>

typedef struct {
    int len;
    char payload[1400];
} msg;

void init(char* remote, int remote_port);
void set_local_port(int port);
void set_remote(char* ip, int port);
int send_message(const msg* m);
int recv_message(msg* r);
msg* receive_message_timeout(int timeout); //timeout in milliseconds
unsigned short crc16_ccitt(const void *buf, int len);

typedef struct {
	unsigned char soh, len, seq, type;
	unsigned char* data;
	unsigned short check;
	unsigned char mark;
} kermit_pack __attribute__((packed));

unsigned short checkSum (kermit_pack *kermit){
	char dataTemp[kermit->len -1];
	dataTemp[0] = kermit->soh;
	dataTemp[1] = kermit->len;
	dataTemp[2] = kermit->seq;
	dataTemp[3] = kermit->type;
	if (kermit->len -5 > 0){
		memcpy(&dataTemp[4], kermit->data, kermit->len -5);
	}
	return crc16_ccitt(dataTemp, kermit->len - 3);
}

//Functia care genereaza orice pachet
kermit_pack* generatePack (char seq, char type, char *data, unsigned char dataSize) {
	kermit_pack* kermit = (kermit_pack*) malloc(sizeof(kermit_pack));
	
	kermit->soh = 0x01;
	kermit->seq = seq%64;
	kermit->type = type;
	kermit->len = 7 + dataSize - 2;
	kermit->mark = 0x0D;

	if (dataSize > 0){
		kermit->data = (unsigned char*) malloc(dataSize);
		memcpy(&kermit->data, &data, dataSize);
	}
	else {
		kermit->data = NULL;
	}

	kermit->check = checkSum(kermit);
	return kermit;
}

// Pentru Debug
void printPack (kermit_pack* kermit, char* sender, char* tip) {
	printf("[%s %s] ", sender, tip);
	printf("Pachetul '%c' cu numarul [%d] : len = %d | crc = %d | soh = %d | mark = %d ~~~ ", 
		kermit->type , kermit->seq ,
		kermit->len, kermit->check,
		kermit->soh, kermit->mark);
	unsigned char size = kermit->len - 5;
	for (int i =0 ; i < size ; i++) {
		printf("%02x ", kermit->data[i]);
	}
	printf("\n\n");
}

// Introduce pachetul kermit intr-un mesaj msg
msg pack (kermit_pack *kermit) {
	msg pack;
	//printPack(kermit, "Trimis", "");

    memcpy (&pack.payload[0], &kermit->soh, 1);
    memcpy (&pack.payload[1], &kermit->len, 1);
    memcpy (&pack.payload[2], &kermit->seq, 1);
    memcpy (&pack.payload[3], &kermit->type, 1);
    memcpy (&pack.payload[4], kermit->data, kermit->len - 5);
    memcpy (&pack.payload[4 + kermit->len - 5], &kermit->check, 2);
    memcpy (&pack.payload[4 + kermit->len - 3], &kermit->mark, 1);
    pack.len = kermit->len + 2;
    return pack;
}

kermit_pack* unpack (msg *x) {
	kermit_pack *kermit = (kermit_pack*) malloc (sizeof(kermit_pack));
	memcpy(kermit, x->payload, 4);
	kermit->data = (unsigned char*) malloc(kermit->len - 5); 

	memcpy(kermit->data, &x->payload[4], kermit->len - 5); // Campul data
	memcpy(&kermit->check, &x->payload[kermit->len -1], 2); // Ultimii 3 bytes
	memcpy(&kermit->mark, &x->payload[kermit->len + 1], 1);
	//printPack(kermit, "Primit", "");
	return kermit;
}

// Intoarce un pachet sosit
kermit_pack* waitForPack (char time) {
	int i;
    msg *r;
	r = receive_message_timeout(time*1000);
    if (r != NULL) {
    	return unpack(r);
    }
    return NULL;
}

void sendPack (kermit_pack *p) {
    msg t = pack(p);
    send_message(&t);
}

kermit_pack* initTransmision (char seq, char type, unsigned char maxl, char time, char npad, char padc, char eol) {
    unsigned char *data = (unsigned char*) malloc(11);
    data[0] = maxl;
    data[1] = time;
    data[2] = npad;
    data[3] = padc;
   	data[4] = eol;
   	memset(&data[5], 0x00, 6);

    kermit_pack* kermit = generatePack(seq, type, data, 11);
    return kermit;
}

#endif

