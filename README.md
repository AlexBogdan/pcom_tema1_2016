=-=-=-=-=-=-=-=-=-==-  Andrei Bogdan Alexandru -=-=-=-=-=-=-=-=-=-
-=-=-=-=-=-=-=-=-=-=-=-=-=- Tema 1 PC -=-=-=-=-=-=-=-=-=-=-=-=-=-=
-=-=-=-=-=-=-=-=-=-=-=-=-=-=- 324CA =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

Implementarea temei a pornit de la definirea structurii de mini-kermit
conform cerintei, cu mentiunea ca vom tine campul de data sub forma de
pointer pentru a aloca aceasta zona de memorie dinamic.
Functiile care prelucreaza pachetele sunt implementate in lib.h pentru
a putea fi folosite si din sender, dar si din receiver.

In ordinea aparitiilor: 
<lib.h>
	-> 'check-sum' = copiaza campurile de care avem nevoie intr-un buffer
	pentr a putea calcula suma de control pentru un pachet kermit;
	-> 'generatePack' = formeaza orice tip de pachet din cerinta.
	-> 'printPack' = functie folosita pentru a printa un pachet kermit
	-> 'pack' = functia care "arhiveaza" un pachet de kermt intr-unul de msg
	-> 'unpack' = functia care va extrage si va forma pachetul kermit dintr-un
	msg
	-> 'waitForPack' = functia care asteapta un pachet din cealalta parte a 
	comunicatiei. In caz ca primeste 3 timeout-uri aceasta va returna null,
	altfel va returna pachetul kermit primit (despachetat in msg)
	-> 'sendPack' = va trimite un pachet kermit incapsulat
	-> 'initTransmision' = transmite parametrii transmisiei

<ksender.c>
	-> 'initSender' = trimite parametrii senderului petnru stabilirea transmisiei
	-> 'sendFileName' = transmite un pachet kermit ce contine un nume de fisier
	-> ~main~ :
		* incercam sa stabilim conexiunea cu emitorul, in caz de fail ne oprim
		* vom transmite numele fiecarui fisier, continutul acestuia si pachetul care
		marcheaza sfarsitul unui fisier
		* la final vom transmite pachetul pentru marcarea incheierii transmisiei

<kreceiver.c>
	-> 'initReceiver' = vom transmite parametrii emitorului pemtru stabilirea transmisie
	-> 'createFile' = vom crea fisierul cu numele pe care tocmai l-am primit
	-> ~main~ :
		* vom incerca stabilirea comunicarii cu senderul
		* vom astepta pachete din partea acestuia pe care le vom trata separat in functie
		de tipul acestora. In cazul in care suma de control este ok atunci vom aplica
		actiunea corespunzatorare tipului de pachet si vom trimite un pachet de ACK, daca 
		nu vom trimite inapoi un pachet de NAK

Oriunde in receiver sau in sender in momentul in care waitForPack va returna NULL inseamna
ca am obitnut un timeout, asa ca vom opri procesele. Vom trata la fiecare pachet primit sau
trimis numarul de secventa conform cerintei.